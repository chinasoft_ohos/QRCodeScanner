/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.blikoon.qrcodescannerlibrary.slice;

import com.blikoon.qrcodescanner.decode.QrManager;
import com.blikoon.qrcodescannerlibrary.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/** QrManager
 *
 * @author ljx
 * @since 2021-04-14
 */
public class MainAbilitySlice extends AbilitySlice {

    private Button button;
    private boolean isFlag = true;
    private CommonDialog dialog;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        button = (Button) findComponentById(ResourceTable.Id_button);

        button.setClickedListener(v -> {
            QrManager.getInstance().startScan(this, new QrManager.OnScanResultCallback() {

                @Override
                public void onScanSuccess(String result) {
                    getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            if (isFlag){
                                show(getContext(),result);
                            }
                            isFlag = false;
                        }
                    });
                }
            });
        });
    }

    private void show(Context context, String text) {
        DirectionalLayout directionalLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_dialog_picker, null, false);
        Text result = (Text) directionalLayout.findComponentById(ResourceTable.Id_tv_result);
        Text confirm = (Text) directionalLayout.findComponentById(ResourceTable.Id_tv_confirm);
        dialog = new CommonDialog(context);
        dialog.setContentCustomComponent(directionalLayout);
        dialog.setAutoClosable(true);
        dialog.setAutoClosable(true);
        dialog.setSize(vp2px(this,340),DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        dialog.setAlignment(LayoutAlignment.CENTER);
        dialog.setCornerRadius(vp2px(this,15));
        dialog.show();
        result.setText(text);
        confirm.setClickedListener(v -> {
            dialog.destroy();
            isFlag = true;
        });
        dialog.setDestroyedListener(new CommonDialog.DestroyedListener() {

            @Override
            public void onDestroy() {
                isFlag = true;
            }
        });
    }

    /**
     * vp转像素
     *
     * @param context
     * @param vp
     * @return int
     */
    private int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
